<?php

namespace Drupal\media_field_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'media_url' formatter.
 *
 * @FieldFormatter(
 *   id = "media_url",
 *   label = @Translation("URL of media object."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MediaUrl extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $media_items = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($media_items)) {
      return $elements;
    }

    /** @var \Drupal\media\MediaInterface[] $media_items */
    foreach ($media_items as $delta => $media) {
      // Only handle media objects.
      if ($media instanceof MediaInterface) {
        // Get the value from the source field.
        $value = $media->getSource()->getSourceFieldValue($media);

        // If this returns a numeric value, it's a file entity's ID.
        if (is_numeric($value)) {
          $file = File::load($value);
          if (!empty($file)) {
            $uri = $file->getFileUri();
            if (!empty($uri)) {
              $value = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
            }
          }
        }

        $elements[$delta] = [
          // '#markup' => $media->getSource()->getSourceFieldValue($media),
          '#type' => 'inline_template',
          '#template' => '{{ value|raw }}',
          '#context' => [
            'value' => $value,
          ],
        ];

        // Add cacheability of each item in the field.
        // $this->renderer->addCacheableDependency($elements[$delta], $media);
      }
    }

    return $elements;
  }

}
