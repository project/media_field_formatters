<?php

namespace Drupal\media_field_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Drupal\media\Entity\MediaType;

/**
 * Plugin implementation of the 'media_file_table' formatter.
 *
 * @FieldFormatter(
 *   id = "media_file_table",
 *   label = @Translation("Table of media files"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MediaTable extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if ($entities = $items->referencedEntities()) {
      $header = [t('Attachment'), t('Size')];
      $rows = [];
      foreach ($entities as $delta => $entity) {
        $source_field_name = $entity->getSource()
          ->getConfiguration()['source_field'];
        /** @var \Drupal\file\FileInterface $file */
        $file = $entity->get($source_field_name)->entity;

        $rows[] = [
          [
            'data' => [
              '#theme' => 'file_link',
              '#file' => $file,
              '#description' => $entity->get($source_field_name)->description,
              '#cache' => [
                'tags' => $file->getCacheTags(),
              ],
            ],
          ],
          ['data' => \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.2.0', fn() => \Drupal\Core\StringTranslation\ByteSizeMarkup::create($file->getSize()), fn() => format_size($file->getSize()))],
        ];
      }

      $elements[0] = [];
      if (!empty($rows)) {
        $elements[0] = [
          '#theme' => 'table__file_formatter_table',
          '#header' => $header,
          '#rows' => $rows,
        ];
      }
    }

    return $elements;
  }

}

